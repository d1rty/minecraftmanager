# Minecraft Manager #

This script provides easy configuration and control over your minecraft servers.

### Installation ###

For installing the Minecraft Manager from a debian package, you have to add my debian repository to your apt sources list:

~~~~
$ sudo echo "deb http://debian.lichtspiele.org/ ./" > /etc/apt/sources.list.d/lichtspiele.org.list
~~~~

Then update your local package repository and install the package:

~~~~
$ sudo apt-get update
$ sudp apt-get install minecraft-manager
~~~~

### Configuration ###

All configuration files reside in `/etc/minecraftmanager/`. Please note that this directory and all it's content (files) must at least be readable by those user(s) that need to control your mincraft servers.

For optimal experience I suggest to create a new user (e.g. `minecraft`) that runs your minecraft servers. Then apply permissions like so:

~~~~
$ sudo find /etc/minecraftmanager -type f | xargs chown minecraft:minecraft
~~~~

#### paths.yaml

This file should be self-explanatory

~~~~
---
# The directory where your minecraft servers reside. Inside this directory all servers have to be in separate directories
servers: /home/minecraft/servers
# The path to java
java_binary: /usr/bin/java
# The path to screen
screen_binary: /usr/bin/screen
# The path to whoami
whoami_binary: /usr/bin/whoami
# The path to taskset (used by Minecraft::Hook::CPUAffinity)
taskset_binary: /usr/bin/taskset
# The path to GNU/grep
grep_binary: /bin/grep
# The path to telnet
telnet_binary: /usr/bin/telnet
~~~~

Please adjust the paths to your needs/situation.

#### servers.yaml

tbd

### Usage ###

The executable program is installed in `/usr/share/minecraftmanager/bin/mcmanager` which has a symlink to `/usr/bin/mcmanager`.

All configured servers can be controlled with the mcmanager command that gives you a good usage help when called without parameters. Besides it also supports bash tab completion.

### Changelog ###

See the [debian changelog](https://bitbucket.org/d1rty/minecraftmanager/src/HEAD/debian/changelog?at=master) for a complete changelog history.