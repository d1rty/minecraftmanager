_minecraft_manager() {

    local cur prev opts base
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    #  The options to complete
    opts="start stop kill help restart status view toolkit list"

    #  Complete the arguments to some of the basic commands.
    case "${prev}" in

        start|stop|restart|kill|status)
            local servers=$(echo -ne "all " && perl -MYAML::Syck -w -e '$r=YAML::Syck::LoadFile("/etc/minecraftmanager/servers.yaml"); print join(" ", keys %$r)')
            COMPREPLY=( $(compgen -W "${servers}" -- ${cur}) )
            return 0
            ;;

        view|toolkit)
            local servers=local servers=$(perl -MYAML::Syck -w -e '$r=YAML::Syck::LoadFile("/etc/minecraftmanager/servers.yaml"); print join(" ", keys %$r)')
            COMPREPLY=( $(compgen -W "${servers}" -- ${cur}) )
            return 0
            ;;

        *)
            ;;
    esac

   COMPREPLY=($(compgen -W "${opts}" -- ${cur}))
   return 0
}
complete -F _minecraft_manager mcmanager