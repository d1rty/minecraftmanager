package ClassHelper;

use strict;
use warnings FATAL => 'all';

use Carp;

sub requireClass {
    my ($class) = @_;

    my $class_name = $class;
    $class_name =~ s{::}{/}g;

    eval { require $class_name.".pm"; };
    confess(sprintf("Could not find object for class %s: %s", $class, $@)) if ($@);
}

sub getObjectFor {
    my ($class, @args) = @_;

    ClassHelper::requireClass($class);
    return $class->new(@args);
}

1;