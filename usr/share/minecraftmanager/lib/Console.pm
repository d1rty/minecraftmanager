package Console;

use strict;
use warnings FATAL => 'all';

use Term::ANSIColor qw(:constants);
use constant INDENT0    => "";
use constant INDENT1    => " ";
use constant INDENT2    => "  ";
use constant INDENT3    => "   ";
use constant INDENT4    => "    ";

use vars qw(@EXPORT);
use Exporter qw(import);
@EXPORT = qw(INDENT0 INDENT1 INDENT2 INDENT3 INDENT4
    CLEAR RESET BOLD DARK UNDERLINE UNDERSCORE BLINK REVERSE
    CONCEALED BLACK RED GREEN YELLOW BLUE MAGENTA CYAN WHITE
    ON_BLACK ON_RED ON_GREEN ON_YELLOW ON_BLUE ON_MAGENTA
    ON_CYAN ON_WHITE
    );

my $config = {
    headers => {
        warn => {
            fh     => *STDERR,
            header => " * WARNING: ",
            color  => BOLD YELLOW,
        },
        info => {
            fh     => *STDOUT,
            header => " * ",
            color => BOLD GREEN,
        },
        note => {
            fh     => *STDOUT,
            header => " * NOTE: ",
            color => BOLD YELLOW,
        },
        error => {
            fh     => *STDERR,
            header => " * ERROR: ",
            color  => BOLD RED,
        },
        no_such_header_error => {
            fh     => *STDERR,
            header => " * UNDEFINED HEADER USED ERROR: ",
            color => BOLD RED,
        },
        write => {
            fh     => *STDOUT,
            header => "",
            color => "",
        },
    },
};

my $header_re = '('.join('|', keys %{$config->{headers}}).')';
$header_re = qr{$header_re}o;

sub AUTOLOAD {
    my ($message, $indent, $color) = @_;

    # get method name
    our $AUTOLOAD;
    my $name = ($AUTOLOAD =~ m{([^:]+$)})[0];

    # set indent level and color
    $indent = INDENT0 if (!defined $indent || $indent =~ m{^$});
    $color  ||= RESET;
    $message||= "";

    my $header_idx = ($name =~ $header_re)[0] || "no_such_header_error";
    #use Data::Dumper; print STDERR Dumper({header_config=> $config->{headers}->{$header_idx}, header_header => $config->{headers}->{$header_idx}->{header}, header_color => $config->{headers}->{$header_idx}->{color}});
    if (-t) {
        print_formatted($config->{headers}->{$header_idx}->{fh}, $indent, $message, $color, $config->{headers}->{$header_idx}->{header}, $config->{headers}->{$header_idx}->{color}, RESET, $name =~ m{.*ln});
    } else {
        print_formatted($config->{headers}->{$header_idx}->{fh}, $indent, $message, "", $config->{headers}->{$header_idx}->{header}, "", "", $name =~ m{.*ln});
    }

}

sub print_formatted {
    my ($fh, $indent, $message, $color, $header, $h_color, $reset, $nl) = @_;
    print $fh $indent;
    print $fh $h_color.$header;
    print $fh $reset;
    print $fh $color.$message;
    print $fh $reset;
    print $fh "\n" if $nl;
}

1;