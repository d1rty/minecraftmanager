package ExecUtil;

=head1 NAME

Util::ExecUtil


=head1 SYNOPSIS

  use Util::ExecUtil;

  my $util = Util::ExecUtil->new();


=head1 DESCRIPTION

This package is used for making superious system calls.

All systemcall commands return STDOUT, STDERR and the exit code inside of a hashref. 


=head1 DEPENDENCIES 

    File::Temp
    File::Path


=head1 ERROR HANDLING

This package uses a L<BaseClass> for error handling. You can trigger errors by calling $class->register_error("string").
If an error can happen, you need to check $class->has_error().
The error message itself can be obtained from $interface->get_error().

=head1 STRIPPED VERSION

This is a stripped version of the original class. It contains only public-usage needed functionality.

=cut

use strict; use warnings;
use BaseClass;
use base qw(BaseClass);

use File::Temp 'tempfile';
use File::Path 'make_path';


=head1 METHODS

=over 4

=item C<configure>

Configure this instance of Util::ExeceUtil. Find out the working environment and set the path where temporary files have to be created.
If the directory does not exist, it will be created.

=back

=cut
sub configure {
    my ($self) = shift;
    $self->{temp_storage_space} = ($^O eq "MSWin32") ? "/temp" : "/tmp";

    make_path($self->{temp_storage_space}, {
        verbose => 0,
        mode    => 0700,
    });
    return 1;
}

=over 4

=item C<_slurp_and_close_filehandle>

  my $stderr = $util->_slurp_and_close_filehandle($filename);

Pseudo-slurps the given filehandle.
Typically this will be STDERR or STDOUT output from a command.
In this case the filehandle represents a temporary file created with File::Temp 'tempfile'.
After slurping the file handle is being closed for cleaning up, and the slurp is being returned.

=back

=cut
sub _slurp_and_close_filehandle {
    my ($self, $fh) = @_;

    my $slurp;
    eval {
        local $/ = undef;
        $slurp = <$fh>; # slurp!
    };
    if ($@) {
        die "Can't read filehandle: $@"
    }
    close $fh;
    return $slurp;
}

=over4

=item C<_register_exec_error>

  $self->_register_exec_error("Description");

Returns a hashref suitable for returning from a redirected exec-method in case of execution errors

=back

=cut
sub _register_exec_error {
    my ($self) = shift;

    return {
        exitcode    => 1,
        success     => 0,
        stdout      => undef,
        stderr      => shift,
    };
}


=over 4

=item C<sudo>

  my $result = $util->execute("command", "arg0", "argN");

Executes some command. Returns a hashref with STDOUT, STDERR and the exitcode

=back

=cut
sub execute {
    my ($self) = shift;
    my (@args) = @_;

    $self->reset_error();
    $self->debug("execute('".join(' ', @args)."') called");

    # =========================================================================
    # Do !!NOT!! create any debug messages from here on.
    # This will have the debug output in the stdout/stderr-result when using the DEBUG-feature!
    # =========================================================================

    # create temporary file for STDOUT and redirect STDOUT to this file
    my $out_fh = tempfile(DIR => $self->{temp_storage_space}, UNLINK => 1);
    open(my $old_outfh, ">&", \*STDOUT) or return $self->_register_exec_error("Can't dup STDOUT: $!");
    open(STDOUT, '>&', $out_fh)         or return $self->_register_exec_error("Can't redirect STDOUT to the temp file: $!");
        $| = 1;

    # create temporary file for STDERR and redirect STDERR to this file
    my $err_fh = tempfile(DIR => $self->{temp_storage_space}, UNLINK => 1);
    open(my $old_errfh, ">&", \*STDERR) or return $self->_register_exec_error("Can't dup STDERR: $!");
    open(STDERR, '>&', $err_fh)         or return $self->_register_exec_error("Can't redirect STDERR to the temp file: $!");
    select(STDERR); $| = 1;

    # run command
    my $exitcode = system(@args);
    $self->register_error($!) if ($exitcode ne "0");

    # restore STDERR and STDOUT
    open(STDOUT, ">&", $old_outfh)      or return $self->_register_exec_error("Can't dup \$old_outfh: $!");
    open(STDERR, ">&", $old_errfh)      or return $self->_register_exec_error("Can't dup \$old_errfh: $!");
        select (STDOUT);

    # seek filehandles to zero location
    seek($out_fh, 0, 0);
    seek($err_fh, 0, 0);

    return {
        exitcode    => $exitcode,
        success     => !$exitcode,
        stdout      => $self->_slurp_and_close_filehandle($out_fh),
        stderr      => $self->_slurp_and_close_filehandle($err_fh),
    };
}


=over 4

=item C<sysexec>

Alias for C<execute()>

=back

=cut
sub sysexec {
    my ($self) = shift;
    my (@args) = @_;

    return $self->execute(@args);
}


=over 4

=item C<sudo>

  my $result = $util->sudo("command", "arg0", "argN");

Executes some command with 'sudo sudocheck'. Returns a hashref with STDOUT, STDERR and the exitcode

=back

=cut
sub sudo {
    my ($self) = shift;
    my (@args) = @_;

    if (scalar @args == 1) {
        return $self->execute("sudo sudocheck ". $args[0]);
    } else {
        unshift(@args, qw(sudo sudocheck));
        return $self->execute(@args);

    }

}


=over 4

=item C<qsudo>

  my $exitcode = $util->qsudo("command", "arg0", "argN");

Returns the exitcode of a command ran with 'sudo sudocheck'

=back

=cut
sub qsudo {
    my ($self) = shift;
    my (@args) = @_;

    return $self->sudo(@args)->{success};
}



=over 4

=item C<file_type>

  my $file = $util->file_type("/directory/file");

Look up the filesystem object given as first paramater and return the type.

=back

=cut
sub file_type {
    my ($self) = shift;
    my ($filename) = shift;

    my $result = $self->sudo("find", "-maxdepth", 0, $filename,
            '(', '-type', 'b', '-and', '-print', 'block_special', ')',
            '(', '-type', 'c', '-and', '-print', 'character_special', ')',
            '(', '-type', 'd', '-and', '-print', 'directory', ')',
            '(', '-type', 'p', '-and', '-print', 'pipe', ')',
            '(', '-type', 'f', '-and', '-print', 'file', ')',
            '(', '-type', 'l', '-and', '-print', 'link', ')',
            '(', '-type', 's', '-and', '-print', 'socket', ')',
            '(', '-type', 'D', '-and', '-print', 'door', ')',
            );

    if ($result->{success}) {
        return $result->{stdout} ? $result->{stdout} : "unknown_type";
    } else {
        return "absent"
    }
}


=head1 AUTHOR

Tristan Cebulla <equinox@lichtspiele.org>


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2011 Tristan Cebulla (<equinox@lichtspiele.org>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut

1;
