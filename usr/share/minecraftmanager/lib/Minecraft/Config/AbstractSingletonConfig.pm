package Minecraft::Config::AbstractSingletonConfig;

use strict;
use warnings FATAL => 'all';

use Class::Singleton;
use base qw/Class::Singleton/;

use YAML::Syck;
use Carp;

sub _new_instance {
    my ($class) = @_;

    my $self = bless {
        file    => $class->FILE(),
        config  => undef,
    }, $class;

    confess sprintf("Cannot read file %s", $self->{file}) unless (-f $self->{file});
    eval {
        $self->{config} = YAML::Syck::LoadFile($self->{file});
    };
    confess sprintf('Could not load file %s: %s', $self->{file}, $@) if ($@);

    $self->init() if ($self->can('init'));
    return $self;
}

sub FILE {
    confess "method FILE must be overwritten";
}

sub exists {
    my ($self, $key) = @_;
    return (grep {$_ eq $key} keys %{$self->all()});
}

sub get {
    my ($self, $key) = @_;
    return $self->{config}->{$key};
}

sub all {
    return shift->{config};
}

1;