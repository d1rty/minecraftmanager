package Minecraft::Config::Paths;

use strict;
use warnings FATAL => 'all';

use Carp;

use Minecraft::Config::AbstractSingletonConfig;
use base qw/Minecraft::Config::AbstractSingletonConfig/;

sub FILE {
    return "/etc/minecraftmanager/paths.yaml";
}

sub init {
    my ($self) = @_;
    foreach (keys %{$self->{config}}) {
        confess(sprintf("%s:init(): configuration key %s does not exist", __PACKAGE__, $_)) if (!-e $self->{config}->{$_});
    }
}

1;