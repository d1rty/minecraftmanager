package Minecraft::Config::Servers;

use strict;
use warnings FATAL => 'all';

use Minecraft::Config::AbstractSingletonConfig;
use base qw/Minecraft::Config::AbstractSingletonConfig/;

use Carp;

sub FILE {
    return "/etc/minecraftmanager/servers.yaml";
}

sub init {
    my ($self) = @_;
    confess(sprintf("%s: No servers defiend. Please add some in %s", __PACKAGE__, $self->FIlE())) unless (scalar keys %{$self->{config}});
}

sub exists {
    my ($self, $server) = @_;
    return ($server eq "all") || (grep {$_ eq $server} keys %{$self->all()});
}

sub get {
    my ($self, $server) = @_;

    confess "Could not return server 'all'" if ($server eq "all");
    confess sprintf("Server %s does not exists", $server) unless $self->exists($server);

    return $self->{config}->{$server};
}

1;