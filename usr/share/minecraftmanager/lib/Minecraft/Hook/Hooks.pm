package Minecraft::Hook::Hooks;

use strict;
use warnings FATAL => 'all';

sub new {
    return bless {}, shift;
}

sub policy {
    die(sprintf("%s must defined it's own policy method", __PACKAGE__));
}

1;