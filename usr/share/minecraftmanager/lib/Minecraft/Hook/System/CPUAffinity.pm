package Minecraft::Hook::System::CPUAffinity;

use strict;
use warnings FATAL => 'all';

use Minecraft::Hook::Hooks;
use base qw/Minecraft::Hook::Hooks/;

use ExecUtil;
use Console;
use Carp;

sub policy {
    return "AFTER";
}

sub run {
    my ($self, $config) = @_;

    foreach (qw(affinity pid)) {
        die(sprintf("%s must be set in server config for feature %s", $_, __PACKAGE__)) unless defined $config->{$_};
    }

    Console::infoln(sprintf("Setting CPU affinity to %s%s%s on PID %s%d%s",
        Console::YELLOW, $config->{affinity}, Console::RESET,
        Console::YELLOW, $config->{pid}, Console::RESET,
    ), Console::INDENT2);

    my $util = ExecUtil->new();
    # -a: operate on all threads for a given PID
    # -p: operate on existing given PID
    # -c: specify CPU affinity settings in list format
    my $result = $util->execute(Minecraft::Config::Paths->instance->get("taskset_binary"), "-apc", $config->{affinity}, $config->{pid});
    Console::errorln(sprintf("Error applying CPU affinity settings on PID %d: %s", $config->{pid}, $result->{stderr})) unless $result->{success};
}

1;