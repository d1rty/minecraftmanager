package Minecraft::Hook::System::RunAs;

## CURRENTLY NOT SUPPORTED

use strict;
use warnings FATAL => 'all';

use Minecraft::Hook::Hooks;
use base qw/Minecraft::Hook::Hooks/;

use ExecUtil;
use Carp;

sub policy {
    return "PREPEND_COMMAND";
}

sub run {
    my ($self, $config) = @_;

    my $util = ExecUtil->new();
    my $result = $util->execute(Minecraft::Config::Paths->instance->get("whoami_binary"));

    confess(sprintf('Could not determine who I am: %s', $result->{stderr})) unless $result->{success};

    chomp($result->{stdout});
    my $iam = $result->{stdout};

    if ( $iam eq "root" || ($iam ne "root" && !-x Minecraft::Config::Paths->instance->get("sudo_binary")) ) {
        return [ "su", "-", $config->{user} ];
    } else {
        return [ "sudo", "su", "-", $config->{user} ];
    }
}

1;