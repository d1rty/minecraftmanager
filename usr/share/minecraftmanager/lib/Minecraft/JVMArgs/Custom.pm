package Minecraft::JVMArgs::Custom;

use strict;
use warnings FATAL => 'all';

use Minecraft::JVMArgs::JVMArgument;
use base qw/Minecraft::JVMArgs::JVMArgument/;

sub args {
    my ($self, $config) = @_;

    my $args = [];
    foreach (keys %$config) {
        push(@$args, sprintf('%s=%s', $_, $config->{$_}));
    }

    return $args;
}

1;