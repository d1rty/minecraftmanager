package Minecraft::JVMArgs::GCLogging;

use strict;
use warnings FATAL => 'all';

use Minecraft::JVMArgs::JVMArgument;
use base qw/Minecraft::JVMArgs::JVMArgument/;

sub args {
    my ($self, $config) = @_;

    return [
        '-Xloggc:gc.log',
        '-XX:+PrintGCDateStamps',
        '-XX:+PrintGCTimeStamps',
        '-XX:+PrintTenuringDistribution',
        '-XX:-PrintGCDetails',
        '-XX:-PrintHeapAtGC',
        '-XX:+PrintAdaptiveSizePolicy',
        '-XX:+PrintPromotionFailure',
        '-XX:-PrintGCApplicationConcurrentTime',
        '-XX:-PrintGCApplicationStoppedTime',
        '-XX:+PrintGCTaskTimeStamps',
        '-XX:-PrintTLAB -XX:+PrintReferenceGC',
    ];
}

1;