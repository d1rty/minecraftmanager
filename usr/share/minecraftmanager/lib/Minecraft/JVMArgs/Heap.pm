package Minecraft::JVMArgs::Heap;

use strict;
use warnings FATAL => 'all';

use Minecraft::JVMArgs::JVMArgument;
use base qw/Minecraft::JVMArgs::JVMArgument/;

sub args {
    my ($self, $config) = @_;

    return [
        sprintf('-Xms%s', $config->{initial}   || '500M'),
        sprintf('-Xmx%s', $config->{maximum}   || '500M'),
    ];
}

1;