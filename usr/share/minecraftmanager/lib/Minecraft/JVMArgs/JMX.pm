package Minecraft::JVMArgs::JMX;

use strict;
use warnings FATAL => 'all';

use Minecraft::JVMArgs::JVMArgument;
use base qw/Minecraft::JVMArgs::JVMArgument/;

sub args {
    my ($self, $config) = @_;

    return [
        sprintf('-Dcom.sun.management.jmxremote.port=%d', $config->{port} || 0),
        '-Dcom.sun.management.jmxremote.ssl=false',
        '-Dcom.sun.management.jmxremote.authenticate=false',
    ];

}

1;