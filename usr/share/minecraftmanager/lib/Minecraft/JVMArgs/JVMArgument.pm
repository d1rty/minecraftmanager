package Minecraft::JVMArgs::JVMArgument;

use strict;
use warnings FATAL => 'all';

sub new {
    return bless {}, shift;
}

sub get {
    my ($self, $config, $joinchar) = @_;
    $joinchar ||= " ";
    my $args = $self->args($config);
    $args = [$args] unless (ref $args eq "ARRAY");
    return join($joinchar, @$args);
}

1;