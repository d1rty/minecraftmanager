package Minecraft::JVMArgs::Jar;

use strict;
use warnings FATAL => 'all';

use Minecraft::JVMArgs::JVMArgument;
use base qw/Minecraft::JVMArgs::JVMArgument/;

sub args {
    my ($self, $config) = @_;

    # TODO: throw exception when jar does not exist

    # the jar path must be relative to the server directory
    # this is usually: /[servers-path]/[server_name]/
    return [
        '-jar',
        $config->{jar}
    ];
}

1;