package Minecraft::JVMArgs::Java8G1Optimization;

use strict;
use warnings FATAL => 'all';

use Minecraft::JVMArgs::JVMArgument;
use base qw/Minecraft::JVMArgs::JVMArgument/;

sub args {
    my ($self, $config) = @_;

    return [
        # try to reduce out of memory errors by using overhead limits
        '-XX:-UseGCOverheadLimit',
        # keep big collects in under 20ms
        '-XX:MaxGCPauseMillis=20',
        # keep small collects in under 20ms
        '-XX:MaxGCMinorPauseMillis=20',

        # FullGC maximum size
        # was -XX:PermSize in Java7
        #'-XX:MetaspaceSize=192M', # you need to set this via the Minecraft::JVMArgs::Custom module

        # G1 garbage collector
        '-XX:+UseG1GC',

        # tbd
        '-XX:SurvivorRatio=3',
        '-XX:G1HeapWastePercent=3',
        '-XX:G1ConfidencePercent=90',
        '-XX:G1HeapRegionSize=4M',
        '-XX:InitiatingHeapOccupancyPercent=80',
        '-XX:+UseAdaptiveGCBoundary',
        '-XX:+NeverTenure',
        '-XX:InitialTenuringThreshold=1',
        '-XX:MaxTenuringThreshold=15',
        '-XX:+ExplicitGCInvokesConcurrent',
        '-XX:+ParallelRefProcEnabled',

        '-XX:+BindGCTaskThreadsToCPUs',
        '-XX:+AggressiveOpts',
        '-XX:SoftRefLRUPolicyMSPerMB=20000',
        '-XX:+CollectGen0First',

        '-XX:+UseFastAccessorMethods',
        '-XX:+UseFastEmptyMethods',
        '-XX:MinHeapFreeRatio=5',
        '-XX:MaxHeapFreeRatio=20'
    ];
}

1;