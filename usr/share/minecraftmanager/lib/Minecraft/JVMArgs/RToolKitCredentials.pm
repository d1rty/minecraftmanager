package Minecraft::JVMArgs::RToolKitCredentials;

use strict;
use warnings FATAL => 'all';

use Minecraft::JVMArgs::JVMArgument;
use base qw/Minecraft::JVMArgs::JVMArgument/;

sub args {
    my ($self, $config) = @_;
    return [ sprintf("%s:%s", $config->{username}, $config->{password}) ];
}

1;