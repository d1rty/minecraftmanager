package Minecraft::Manager;

use strict;
use warnings FATAL => 'all';

use Text::Table;
use Carp;

use Minecraft::Config::Paths;
use ClassHelper;
use Console;

sub _get_config_for {
    my ($name) = @_;
    confess("No server name given") if (!defined $name || $name eq "");
    return Minecraft::Config::Servers->instance->get($name);
}

sub list {
    my $table = Text::Table->new(
        sprintf("%sName", Console::BOLD),
        "Session",
        "Running",
        "Server",
        sprintf("Running%s", Console::RESET)
    );
    my $servers = Minecraft::Config::Servers->instance()->all();

    foreach (keys %$servers) {
        my $session_class   = $servers->{$_}->{session}->{class};
        my $server_class    = $servers->{$_}->{server}->{class};

        my $session = ClassHelper::getObjectFor($session_class, $_);
        my $server  = ClassHelper::getObjectFor($server_class,  $_, $session);

        my $session_running = $session->is_running();
        my $server_running  = $server->is_running();

        $table->load( [
            sprintf("%s%s%s", Console::BRIGHT_GREEN, $_, Console::RESET),
            sprintf("%s%s%s", Console::YELLOW, $session_class, Console::RESET),
            sprintf("%s%s%s%s", 
                ($session_running) ? Console::BRIGHT_GREEN : Console::RED,
                ($session_running) ? "yes" : "no",
                Console::RESET,
                ($session_running) ? sprintf(" (PID %d)", $session->pid()) : ""
            ),
            sprintf("%s%s%s", Console::YELLOW, $server_class, Console::RESET),
            sprintf("%s%s%s%s", 
                ($server_running) ? Console::BRIGHT_GREEN : Console::RED,
                ($server_running) ? "yes" : "no",
                Console::RESET,
                ($server_running) ? sprintf(" (PID %d)", $server->pid()) : ""
            )
        ] );
    }

    print $table;
}


sub start {
    my ($name) = @_;

    my $config = _get_config_for($name);

    my $session = ClassHelper::getObjectFor($config->{session}->{class}, $name);
    my $server  = ClassHelper::getObjectFor($config->{server}->{class}, $name, $session);
    $session->start();
    $server->start();
}

sub stop {
    my ($name) = @_;

    my $config = _get_config_for($name);

    my $session = ClassHelper::getObjectFor($config->{session}->{class}, $name);
    my $server  = ClassHelper::getObjectFor($config->{server}->{class}, $name, $session);
    $server->stop();

    if (!defined $config->{session}->{keep_on_stop} || !$config->{session}->{keep_on_stop}) {
        $session->stop();
    }
}

sub kill {
    my ($name) = @_;

    my $config = _get_config_for($name);

    my $session = ClassHelper::getObjectFor($config->{session}->{class}, $name);
    my $server  = ClassHelper::getObjectFor($config->{server}->{class}, $name, $session);
    $server->kill();
    $session->stop();
}

sub status {
    my ($name) = @_;

    if (!defined $name || $name eq 'all') {
        Minecraft::Manager::list();
        return;
    }

    my $config = _get_config_for($name);

    my $session = ClassHelper::getObjectFor($config->{session}->{class}, $name);
    my $server  = ClassHelper::getObjectFor($config->{server}->{class}, $name, $session);

    Console::infoln(sprintf("Server Name: %s%s%s", Console::BRIGHT_GREEN, $name, Console::RESET));

    my $table = Text::Table->new("  ", "", "", "", "");

    # session stuff
    my $session_running = $session->is_running();
    $table->load( [
        "",
        sprintf("%sScreen Session Type%s", Console::BOLD, Console::RESET),
        sprintf("%s%s%s", Console::YELLOW, $config->{session}->{class}, Console::RESET),
        sprintf("%s%s%s", 
            ($session_running) ? Console::BRIGHT_GREEN : Console::RED, 
            ($session_running) ? "running" : "not running",
            Console::RESET
        ),
        ($session_running) ? sprintf("(PID %d)", $session->pid()) : ""
    ] );

    # server stuff
    my $server_running  = $server->is_running();
    $table->load( [
        "",
        sprintf("%sServer Type%s", Console::BOLD, Console::RESET),
        sprintf("%s%s%s", Console::YELLOW, $config->{server}->{class}, Console::RESET),
        sprintf("%s%s%s", 
            ($server_running) ? Console::BRIGHT_GREEN : Console::RED, 
            ($server_running) ? "running" : "not running",
            Console::RESET
        ),
        ($server_running) ? sprintf("(PID %d)", $server->pid()) : ""
    ] );
    print $table;

    Console::writeln("");

    if ($session_running) {
        Console::writeln("To connect to the screen session type:", Console::INDENT3);
        Console::writeln(sprintf(" > %s%s view %s%s", Console::YELLOW, $0, $name, Console::RESET), Console::INDENT3);
    }

    if ($server_running && $config->{server}->{class} eq "Minecraft::Server::RToolKitServer") {
        Console::writeln("To connect to the Remote ToolKit Session type:", Console::INDENT3);
        Console::writeln(sprintf(" > %s%s toolkit %s%s", Console::YELLOW, $0, $name, Console::RESET), Console::INDENT3);
    }
}

sub view {
    my ($name) = @_;
    my $config = _get_config_for($name);

    die(sprintf('Server %s has no attachable session', $name)) unless ($config->{session}->{class} eq 'Session::Screen');

    my $session = ClassHelper::getObjectFor($config->{session}->{class}, $name);
    $session->view();
}

sub toolkit {
    my ($name) = @_;
    my $config = _get_config_for($name);

    die(sprintf('Server %s%s%s does not support RemoteToolkit', Console::BRIGHT_GREEN, $name, Console::RESET))
        unless ($config->{server}->{class} eq 'Minecraft::Server::RToolKitServer');

    my $session = ClassHelper::getObjectFor($config->{session}->{class}, $name);
    my $server  = ClassHelper::getObjectFor($config->{server}->{class}, $name, $session);
    $server->toolkit();
}

1;
