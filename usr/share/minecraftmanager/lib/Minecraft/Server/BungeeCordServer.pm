package Minecraft::Server::BungeeCordServer;

use strict;
use warnings FATAL => 'all';

use Minecraft::Server::GenericServer;
use base qw/Minecraft::Server::GenericServer/;

sub commands {
    return {
        stop    => 'end'
    };
};

1;