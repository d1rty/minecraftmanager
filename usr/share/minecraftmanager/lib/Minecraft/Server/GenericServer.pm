package Minecraft::Server::GenericServer;
use strict;
use warnings FATAL => 'all';

use ExecUtil;
use Carp;
use Time::HiRes qw(usleep);

sub new {
    my ($class, $name, $session) = @_;

    my $self = bless {
        name        => $name,
        session     => $session,
        config      => Minecraft::Config::Servers->instance()->get($name),
        home        => sprintf('%s/%s', Minecraft::Config::Paths->instance()->get('servers'), $name),
        command     => undef,
        executil    => ExecUtil->new(),
    }, $class;

    $self->buildCommand();

    return $self;
}

sub commands {
    confess(sprintf("%s must implements it's own commands() method"), __PACKAGE__);
}

sub buildCommand {
    my ($self) = @_;

    confess("Minecraft::JVMArgs::Jar is missing in the servers args configuration")
        if (!(grep $_ eq 'Minecraft::JVMArgs::Jar', @{$self->{config}->{server}->{args}}));

    my $command =
        Minecraft::Config::Paths->instance()->get("java_binary") .
        sprintf(" -DMMServer=%s", $self->{name});

    foreach (@{$self->{config}->{server}->{args}}) {
        my $class = ClassHelper::getObjectFor($_);
        $command .= " " . $class->get($self->{config}->{server}->{$_} || {});

    }

    $self->{command} = $command;
}

sub pid {
    my ($self) = @_;

    my $pcmd = sprintf('ps ux | grep -E "\-[D]MMServer=%s" | awk "{print \$2}"', $self->{name});
    my $result = $self->{executil}->execute($pcmd);

    chomp($result->{stdout});
    return undef if (!$result->{success} || $result->{stdout} eq "");
    return $result->{stdout};
}

sub is_running {
    my ($self) = @_;
    return (defined $self->pid());
}

sub start {
    my ($self) = @_;

    # check home directory
    die(sprintf("Directory %s%s%s for server %s%s%s does not exist",
        Console::BRIGHT_GREEN, $self->{home}, Console::RESET,
        Console::BRIGHT_GREEN, $self->{name}, Console::RESET
    )) unless (-d $self->{home});

    if ($self->is_running) {
        Console::warnln(sprintf("Server %s%s%s is already running", Console::BRIGHT_GREEN, $self->{name}, Console::RESET));
        return;
    }

    $self->before_start() if ($self->can('before_start'));

    Console::info(sprintf("Starting server %s%s%s: ", Console::BRIGHT_GREEN, $self->{name}, Console::RESET));

    $self->run_hooks("BEFORE");

    # clean up session and begin from scratch - sleep 0.3s between each command
    $self->{session}->execute('clear');
    usleep(0.3);
    $self->{session}->execute(sprintf('cd %s', $self->{home}));
    usleep(0.3);
    $self->{session}->execute($self->{command});

    $| = 1; # unbuffer output
    my @spinState = ("-", "\\", "|", "/");

    my $i = 0;
    while (1) {
        $i++;
        printf "[%s]", $spinState[$i % @spinState];
        Time::HiRes::sleep(0.05);
        print "\b\b\b";  # backspace

        # wait at least 3 seconds
        next if ($i < 60);
        last if ($self->is_running() || $i > 200);
    }

    if (!$self->is_running()) {
        Console::writeln();
        Console::warnln("Cannot find process in processtable. Please find out what happened:");
        Console::writeln(sprintf("> %s%s view %s%s", Console::BRIGHT_YELLOW, $0, $self->{name}, Console::RESET), Console::INDENT3);
        confess("Error starting server");
    }

    Console::writeln(sprintf("done (PID %d)", $self->pid()));

    $self->run_hooks("AFTER");

    $self->after_start() if ($self->can('after_start'));
}

sub run_hooks {
    my ($self, $policy) = @_;

    $self->{config}->{server}->{hooks} = [] unless ref $self->{config}->{server}->{hooks} eq "ARRAY";

    foreach (@{$self->{config}->{server}->{hooks}}) {
        if ($_ eq 'System::RunAs') {
            Console::warnln("The hook System::RunAs feature is currently not supported");
            next;
        }

        my $class = ClassHelper::getObjectFor($_);
        my $class_policy = $class->policy();
        next unless ($class_policy eq $policy);

        my $config = $self->{config}->{server}->{$_};
        $config->{pid} = $self->pid();

        $class->run($config);
    }
}

sub stop {
    my ($self) = @_;

    if (!$self->is_running) {
        Console::infoln(sprintf("Server %s%s%s is not running", Console::BRIGHT_GREEN, $self->{name}, Console::RESET));
        return;
    }

    Console::info(sprintf("Stopping server %s%s%s (PID %d): ", Console::BRIGHT_GREEN, $self->{name}, Console::RESET, $self->pid()));

    $self->before_stop() if ($self->can('before_stop'));

    $self->{session}->execute($self->commands()->{stop});

    $| = 1; # unbuffer output
    my @spinState = ("-", "\\", "|", "/");

    my $i = 0;
    while (1) {
        $i++;
        printf "[%s]", $spinState[$i % @spinState];
        Time::HiRes::sleep(0.05);
        print "\b\b\b";  # backspace

        # wait at least 3 seconds
        next if ($i < 60);
        last if (!$self->is_running() || $i > 600);
    }

    if ($self->is_running()) {
        Console::writeln();
        Console::warnln("Process is remaining in processtable. Please find out what happened:");
        Console::writeln(sprintf("> %sps faux ww | grep '%d'%s", Console::BRIGHT_YELLOW, $self->pid(), Console::RESET), Console::INDENT3);
        die("Error stopping server");
    }

    Console::writeln("done");

    $self->after_stop() if ($self->can('after_stop'));
}

sub kill {
    my ($self, $nowarn) = @_;
    $nowarn = 0 unless defined $nowarn;;

    if (!$self->is_running) {
        Console::infoln(sprintf("Server %s%s%s is not running", Console::BRIGHT_GREEN, $self->{name}, Console::RESET));
        return;
    }

    if (!$nowarn) {
        Console::warnln(sprintf("You are about to send a SIGINT signal to the server %s%s%s. Is this what you want to do?", Console::BRIGHT_GREEN, $self->{name}, Console::RESET));
        Console::writeln(sprintf("Press ENTER to continue or %sCtrl + c%s to abort", Console::RED, Console::RESET), Console::INDENT3);
        <STDIN>;
    }

    Console::info(sprintf("Slaying server %s%s%s (PID %d): ", Console::BRIGHT_GREEN, $self->{name}, Console::RESET, $self->pid()));

    $self->before_stop() if ($self->can('before_stop'));

    $self->{executil}->execute("kill", -9, $self->pid());

    $| = 1; # unbuffer output
    my @spinState = ("-", "\\", "|", "/");

    my $i = 0;
    while (1) {
        $i++;
        printf "[%s]", $spinState[$i % @spinState];
        Time::HiRes::sleep(0.05);
        print "\b\b\b";  # backspace

        # wait at least 3 seconds
        next if ($i < 60);
        last if (!$self->is_running() || $i > 600);
    }

    if ($self->is_running()) {
        Console::writeln();
        Console::warnln("Process is remaining in processtable. Please find out what happened:");
        Console::writeln(sprintf("> %sps faux ww | grep '%d'%s", Console::BRIGHT_YELLOW, $self->pid(), Console::RESET), Console::INDENT3);
        die("Error stopping server");
    }

    Console::writeln("done");

    $self->after_stop() if ($self->can('after_stop'));
}

1;