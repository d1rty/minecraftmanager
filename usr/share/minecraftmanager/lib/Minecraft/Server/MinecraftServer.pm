package Minecraft::Server::MinecraftServer;

use strict;
use warnings FATAL => 'all';

use Minecraft::Server::GenericServer;
use base qw/Minecraft::Server::GenericServer/;

sub commands {
    return {
        stop    => 'stop',
    };
};

1;