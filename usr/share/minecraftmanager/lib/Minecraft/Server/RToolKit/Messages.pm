package Minecraft::Server::RToolKit::Messages;

use strict;
use warnings FATAL => 'all';

use Minecraft::Server::RToolKit::Properties;
use base qw/Minecraft::Server::RToolKit::Properties/;

use YAML::Syck;

sub filename {
    return "messages.txt";
}

sub node {
    return "wrapper";
}

sub properties {
    my ($self, $config) = @_;

    my $file = "/etc/minecraftmanager/toolkit.messages.yaml";
    confess(sprintf("File %s not found. Please reinstall", $file)) unless (-f $file);

    my $properties;
    eval {
        $properties = YAML::Syck::LoadFile($file);
    };
    confess(sprintf("Error loading file %s: %s", $file, $@)) if ($@);

    return $properties;
}

1;