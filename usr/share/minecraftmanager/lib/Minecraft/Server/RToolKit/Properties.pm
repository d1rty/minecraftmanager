package Minecraft::Server::RToolKit::Properties;

use strict;
use warnings FATAL => 'all';

use ExecUtil;
use Carp;
use File::Path qw/mkpath/;

sub new {
    my ($class, $server) = @_;

    my $self = bless {
        class       => $class,
        executil    => ExecUtil->new(),
        server      => $server,
    }, $class;

    return $self;
}

sub filename() {
    confess(sprintf("%s must implement it's own filename method", __PACKAGE__));
}

sub node() {
    confess(sprintf("%s must implement it's own node method", __PACKAGE__));
}

sub properties() {
    confess(sprintf("%s must implements it's own properties method", __PACKAGE__));
}

sub run {
    my ($self) = @_;

    my $node        = $self->node();
    my $properties  = $self->properties($self->{server}->{config}->{$node});
    my $config      = {};

    foreach (keys %$properties) {
        $config->{$_} = (defined $self->{server}->{config}->{$node}->{$self->{class}}->{$_})
            ? $self->{server}->{config}->{$node}->{$self->{class}}->{$_}
            : $properties->{$_}
    }

    $self->create_properties($config);
}

sub create_properties {
    my ($self, $data) = @_;

    $data = {} unless (ref $data eq "HASH");
    my
        $path = sprintf("%s/toolkit", $self->{server}->{home});
    eval { mkpath($path); };
    confess ($@) if ($@);

    my $fq_filename = sprintf("%s/%s", $path, $self->filename());
    open(my $fh, '>', $fq_filename) or confess(sprintf("Could not write to file %s: %s", $fq_filename, $!));
    print $fh sprintf(
        "%s=%s\n",
        $_,
        (defined $data->{$_}) ? $data->{$_} : ""
    ) foreach (keys %$data);
    close $fh;
}


1;