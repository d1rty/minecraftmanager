package Minecraft::Server::RToolKit::RemoteProperties;

use strict;
use warnings FATAL => 'all';

use Minecraft::Server::RToolKit::Properties;
use base qw/Minecraft::Server::RToolKit::Properties/;

use YAML::Syck;

sub filename {
    return "remote.properties";
}

sub node {
    return "server";
}

sub properties {
    my ($self, $config) = @_;

    # load defaults
    my $file = "/etc/minecraftmanager/toolkit.remote.properties.yaml";
    confess(sprintf("File %s not found. Please reinstall", $file)) unless (-f $file);

    my $properties;
    eval {
        $properties = YAML::Syck::LoadFile($file);
    };
    confess(sprintf("Error loading file %s: %s", $file, $@)) if ($@);

    return $properties;
}

1;