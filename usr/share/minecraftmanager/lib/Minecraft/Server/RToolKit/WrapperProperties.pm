package Minecraft::Server::RToolKit::WrapperProperties;

use strict;
use warnings FATAL => 'all';

use Minecraft::Server::RToolKit::Properties;
use base qw/Minecraft::Server::RToolKit::Properties/;

use Carp;
use YAML::Syck;

sub filename {
    return "wrapper.properties";
}

sub node {
    return "toolkit";
}

sub properties {
    my ($self, $config) = @_;

    # load defaults
    my $file = "/etc/minecraftmanager/toolkit.wrapper.properties.yaml";
    confess(sprintf("File %s not found. Please reinstall", $file)) unless (-f $file);

    my $properties;
    eval {
        $properties = YAML::Syck::LoadFile($file);
    };
    confess(sprintf("Error loading file %s: %s", $file, $@)) if ($@);

    # special handling for runtime arguments
    if (!defined $properties->{'extra-runtime-arguments'} || $properties->{'extra-runtime-arguments'} eq "") {
        $properties->{'extra-runtime-arguments'} = [];
    } else {
        $properties->{'extra-runtime-arguments'} = [ $properties->{'extra-runtime-arguments'} ];
    }

    # args classes from toolkit config
    $config->{args} = [] unless (ref $config->{args} eq "ARRAY");
    foreach (@{$config->{args}}) {

        if ($_ eq 'Minecraft::JVMArgs::Jar') {
            my $class = ClassHelper::getObjectFor($_);
            $properties->{'minecraft-server-jar'} = $class->args($config->{$_})->[1];

        } elsif ($_ eq 'Minecraft::JVMArgs::Heap') {
            my $class = ClassHelper::getObjectFor($_);
            my $args = $class->args($config->{$_});

            foreach my $heap_settings (@$args) {
                if ($heap_settings =~ m{^\-Xms(.+)$}) {
                    $properties->{'initial-heap-size'} = $1;
                }
                if ($heap_settings =~ m{^\-Xmx(.+)$}) {
                    $properties->{'maximum-heap-size'} = $1;
                }
            }

        } else {
            my $class = ClassHelper::getObjectFor($_);
            push @{$properties->{'extra-runtime-arguments'}}, $class->get($config->{$_}, ",");
        }
    }

    # make extra-runtime-arguments a string again and escape equal signs
    $properties->{'extra-runtime-arguments'} = join(",", @{$properties->{'extra-runtime-arguments'}});
    $properties->{'extra-runtime-arguments'} =~ s{=}{\\=}g;

    return $properties;
}

1;