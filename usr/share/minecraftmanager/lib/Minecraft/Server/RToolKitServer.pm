package Minecraft::Server::RToolKitServer;

use strict;
use warnings FATAL => 'all';

use Minecraft::Server::GenericServer;
use base qw/Minecraft::Server::GenericServer/;

use Carp;
use Socket;
require 'sys/ioctl.ph';

use Console;
use Minecraft::Config::Paths;
use Minecraft::Server::RToolKit::WrapperProperties;
use Minecraft::Server::RToolKit::RemoteProperties;
use Minecraft::Server::RToolKit::Messages;
use Minecraft::Config::Paths;

sub commands {
    return {
        stop    => '.stopwrapper',
    };
};

sub run_hooks {
    my ($self, $policy) = @_;

    $self->{config}->{toolkit}->{hooks} = [] unless ref $self->{config}->{toolkit}->{hooks} eq "ARRAY";

    foreach (@{$self->{config}->{toolkit}->{hooks}}) {
        if ($_ eq 'System::RunAs') {
            Console::warnln("The hook System::RunAs feature is currently not supported");
            next;
        }

        my $class = ClassHelper::getObjectFor($_);
        my $class_policy = $class->policy();
        next unless ($class_policy eq $policy);

        my $config = $self->{config}->{toolkit}->{$_};
        $config->{pid} = $self->pid();

        $class->run($config);
    }
}

sub before_start() {
    my ($self) = @_;

    Console::infoln("Setting up Remote ToolKit");

    $self->_install_toolkit();

    # setup files
    my $remote = Minecraft::Server::RToolKit::RemoteProperties->new($self);
    $remote->run();

    my $wrapper = Minecraft::Server::RToolKit::WrapperProperties->new($self);
    $wrapper->run();

    my $messages = Minecraft::Server::RToolKit::Messages->new($self);
    $messages->run();
}

sub _install_toolkit {
    my ($self) = @_;

    my $cmd = sprintf("cp -avr /opt/minecraftmanager/rtoolkit/* %s/", $self->{home});
    my $result = $self->{executil}->execute($cmd);

    confess (sprintf("Error copying Remote ToolKit files: %s", $result->{stderr})) unless $result->{success};
}

sub get_interface_address {
    my ($self, $iface) = @_;
    my $socket;
    socket($socket, PF_INET, SOCK_STREAM, (getprotobyname('tcp'))[2]) || confess "unable to create a socket: $!";
    my $buf = pack('a256', $iface);
    if (ioctl($socket, SIOCGIFADDR(), $buf) && (my @address = unpack('x20 C4', $buf))) {
        return join('.', @address);
    }
    return undef;
}

sub toolkit {
    my ($self) = @_;

    if (!$self->is_running()) {
        Console::infoln(sprintf("Server %s%s%s is not running", Console::BRIGHT_GREEN, $self->{name}, Console::RESET));
        return;
    }

    # get settings from current rtoolkit configuration
    my $remote_properties_file = sprintf("%s/toolkit/remote.properties", $self->{home});
    my $result = $self->{executil}->execute(
        Minecraft::Config::Paths->instance->get("grep_binary"),
        "remote",
        $remote_properties_file
    );
    confess(sprintf("Error while searching for remote settings in %s: %s", $remote_properties_file, $result->{stderr})) unless $result->{success};

    my $remote_settings = {};
    foreach my $line (split(/\n/, $result->{stdout})) {
        if ($line =~ m{^remote-bind-address=(.+)$}) {
            $remote_settings->{host} = $1;

        } elsif ($line =~ m{^remote-control-port=(.+)$}) {
            $remote_settings->{port} = $1;
        }
    }
    $remote_settings->{host} = $self->get_interface_address('eth0') unless
        (defined $remote_settings->{host} && $remote_settings->{host} ne "");


    if (!defined $remote_settings->{host} || $remote_settings->{host} eq "") {
        confess('Could not reliably find out the hosts IP adress of interface eth0');
    }

    if (!defined $remote_settings->{port}) {
        Console::errorln(sprintf("Server %s%s%s has no remote port configured", Console::BRIGHT_GREEN, $self->{name}, Console::RESET));
        return;
    }

    Console::infoln(sprintf("Entering RToolKit session. Hit %sCtrl + a + d%s to leave the session.", Console::YELLOW, Console::RESET));
    Console::writeln("You can reach the RToolKit Console also via telnet:", Console::INDENT3);
    Console::writeln(sprintf("> %s%s %s %d%s",
        Console::YELLOW,
        Minecraft::Config::Paths->instance()->get("telnet_binary"),
        $remote_settings->{host}, $remote_settings->{port},
        Console::RESET
    ), Console::INDENT3);
    Console::writeln("");
    Console::writeln(sprintf("Press ENTER to continue or %sCtrl + c%s to abort", Console::RED, Console::RESET), Console::INDENT3);
    <STDIN>;

    system(sprintf('%s %s %s', Minecraft::Config::Paths->instance()->get("telnet_binary"), $remote_settings->{host}, $remote_settings->{port}));
}

1;