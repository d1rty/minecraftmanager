package Session::Screen;

use strict;
use warnings FATAL => 'all';

use Carp;
use Time::HiRes qw(usleep);

use ExecUtil;
use Console;
use Minecraft::Config::Paths;

sub new {
    my ($class, $name) = @_;
    confess "No screen name provided" unless defined $name;

    return bless {
        name        => $name,
        executil    => ExecUtil->new(),
    }, $class;
}

sub start {
    my ($self) = @_;
    if ($self->is_running()) {
        Console::infoln(sprintf("Screen %s%s%s is already running", Console::BRIGHT_GREEN, $self->{name}, Console::RESET));
        return;
    }

    Console::info(sprintf("Starting screen %s%s%s: ", Console::BRIGHT_GREEN, $self->{name}, Console::RESET));
    my $result = $self->{executil}->execute("screen", "-dmS", $self->{name});

    if (!$result->{success}) {
        Console::writeln("failed");
        Console::errorln(sprintf("Error starting screen %s%s%s: %s", Console::BRIGHT_GREEN, $self->{name}, Console::RESET, $result->{stderr}));
        return;
    }

    Console::writeln(sprintf("done (PID %d)", $self->pid()));
}

sub stop {
    my ($self) = @_;
    if (!$self->is_running()) {
        Console::infoln(sprintf("Screen %s%s%s is not running", Console::BRIGHT_GREEN, $self->{name}, Console::RESET));
        return;
    }
    my $pid = $self->pid();

    Console::info(sprintf("Stopping screen %s%s%s (PID %d): ", Console::BRIGHT_GREEN, $self->{name}, Console::RESET, $pid));
    my $result = $self->{executil}->execute("kill", "-9", $pid);

    $| = 1; # unbuffer output
    my @spinState = ("-", "\\", "|", "/");

    my $i = 0;
    while (1) {
        $i++;
        printf "[%s]", $spinState[$i % @spinState];
        Time::HiRes::sleep(0.05);
        print "\b\b\b";  # backspace

        # wait at least 1 second
        next if ($i < 20);

        if (!$result->{success}) {
            Console::writeln("failed");
            Console::errorln(sprintf("Could not stop screen %s%s%s: %s", Console::BRIGHT_GREEN, $self->{name}, Console::RESET, $result->{stderr}));
            last;
        }

        if (!$self->is_running()) {
            Console::writeln("done");
            last;
        }

        last if ($self->is_running() || $i > 200);
    }

    if ($self->is_running()) {
        Console::errorln(sprintf("Could not stop screen %s%s%s: Please investigate",
            Console::BRIGHT_GREEN, $self->{name}, Console::RESET
        ));
    }

    # wipe dead screens
    $self->{executil}->execute(Minecraft::Config::Paths->instance()->get("screen_binary"), -"wipe");
}

sub pid {
    my ($self) = @_;

    my $pid = $self->{executil}->execute(sprintf("ps ux | grep '[S]CREEN -dmS %s' | awk '{print \$2}'", $self->{name}));
    chomp($pid->{stdout});
    return ($pid->{stdout} eq "") ? undef : $pid->{stdout} ;
}

sub is_running {
    my ($self) = @_;
    return (defined $self->pid());
}

sub view {
    my ($self) = @_;

    if (!$self->is_running()) {
        Console::infoln(sprintf("Screen %s%s%s is not running", Console::BRIGHT_GREEN, $self->{name}, Console::RESET));
        return;
    }

    Console::infoln(sprintf("Entering screen session. Hit %sCtrl + a + d%s to detach from the screen.", Console::YELLOW, Console::RESET));
    Console::writeln(sprintf("Press ENTER to continue or %sCtrl + c%s to abort", Console::RED, Console::RESET), Console::INDENT3);
    <STDIN>;

    system(sprintf('%s -x %s', Minecraft::Config::Paths->instance()->get("screen_binary"), $self->{name}));
}

sub execute {
    my ($self, $command) = @_;

    # since screen is not capable of executing commands with more than 64 args or 512 characters, we need to create a
    # file that executes this command for us.
    my $filename = sprintf('%s/%s/.mcmanager_start_server.sh', Minecraft::Config::Paths->instance()->get('servers'), $self->{name});

    open (my $fh, '>', $filename) or return {
        success => 0,
        stderr  => $!
    };
    print $fh "#!/bin/bash\n";
    print $fh sprintf("%s\n", $command);
    close $fh;

    my @cmd = (
        Minecraft::Config::Paths->instance()->get("screen_binary"),
        "-S", $self->{name},
        "-p", "0",
        "-X", "stuff",
        # force a newline (enter command)
        sprintf "/bin/bash %s
", $filename
    );

    system(@cmd);
    if ($? == -1) {                                                                                                                                                                               return {
            success => 0,                                                                                                                                                                             stderr  => sprintf("Failed to execute command: %s", $!)
        };
    } elsif ($? & 127) {
        return {
            success => 0,
            stderr  => sprintf("Child died with signal %d, %s coredump",
                ($? & 127),
                ($? & 128) ? 'with' : 'without'
            )
        };
    }

    return { success => 1 };
}

1;