package Session::Terminal;

use strict;
use warnings FATAL => 'all';

# CURRENTLY NOT SUPPORTED

use ExecUtil;
use Carp;

sub new {
    my ($class, $name) = @_;
    confess "No screen name provided" unless defined $name;

    return bless {
        name        => $name,
        executil    => ExecUtil->new(),
    }, $class;
}

sub start {
    # noop
}

sub stop {
    # noop
}

sub execute {
    my ($self, $command) = @_;


    print
        "executing cmd: $command";
    system($command);

    #my $result = $self->{executil}->execute($command);
    #confess(sprintf("Could not execute command: %s", $result->{stderr})) unless $result->{success};
    return {
        success => 1,
    };
}

1;